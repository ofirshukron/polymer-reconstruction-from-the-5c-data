function [result,params] = SimulateRandomLoopModel(params)
% Simulate a generalized Rouse polymer with connectors corresponding to peaks of the CC data
% Add random connectors between monomers in 2 TAD structures. 
% codes written by Ofir Shukron, Ecole Normale Superieure,  46 rue d'Ulm, Paris, France

% % Load the pre-analyzed experimental data 
load('savedAnalysisTADDAndE.mat');

% Load system parameters if not supplied
if ~exist('params','var')% allow input params
    [params] = RandomLoopSystemParameters;    % generate system parameters
end

% Generate result structure for all tested connectivity
[result] = GenerateRandomLoopResultStruct(params);

% Run Simulations
for c1Idx = 1:numel(params.connectivityTAD1)
    for c2Idx = 1:numel(params.connectivityTAD2)
        
        %-- Preallocations        
        % create a matrix to hold the nearest neighbor monomers square distance 
        nnDist = zeros(params.numBeads-1,1);
        if params.calculateMSD
            msd     = zeros(params.numBeads, params.numSteps+1);
            msdTemp = msd; % for each simulation
        end
        
        % create a matrix to hold the value of radius of gyration
        srog      = zeros(1,params.numSimulations);  
        
        for rIdx = 1:params.numSimulations
            sprintf('Simulation round %d  (%d/%d)\n', rIdx,c1Idx,c2Idx)
            
            % Create chain position .txt file
            if params.saveChainPosition
                % create a folder for each simulation run
                [~,~,~]= mkdir(fullfile(params.resultFolder,params.trajectoriesFolder,['C_',num2str(c1Idx),'_',num2str(c2Idx)]));
                posBaseFileName = fullfile(params.resultFolder,params.trajectoriesFolder,['C_',num2str(c1Idx),'_',num2str(c2Idx)],...
                    sprintf('%s%s%u%s',params.trajectoriesBaseName,'_simulation_',rIdx,'.dat')); % file name to save position of the chain
                posfName        = fopen(posBaseFileName,'wt');% create the file
            end
            
            % Initialize the RCL polymer
            matTAD1 = GenerateRandomConnectivityMatrix(result(c1Idx,c2Idx).numConnectorsTAD1,numel(params.beadRange.TAD1));
            matTAD2 = GenerateRandomConnectivityMatrix(result(c1Idx,c2Idx).numConnectorsTAD2,numel(params.beadRange.TAD2));
            
            % Record the average connectivity 
            result(c1Idx,c2Idx).averageMonomerConnectivityTAD1 = ((rIdx-1)*result(c1Idx,c2Idx).averageMonomerConnectivityTAD1+mean(sum(-matTAD1,1)/(params.beadRange.TAD1(end)-2)))/rIdx;
            result(c1Idx,c2Idx).averageMonomerConnectivityTAD2 = ((rIdx-1)*result(c1Idx,c2Idx).averageMonomerConnectivityTAD2+mean(mean(-matTAD2)))/rIdx;
            
            % Construct the polymer graph laplacian 
            R       = InitializeConnectivityMatrix(params,matTAD1,matTAD2);

            % Initialize chain position
            pos        = InitializeChainPosition(params);
            
            % Set number of steps by relaxation time if this option is set to
            % True in parameters
            result(c1Idx,c2Idx).params = DetermineStepsByRelaxationTime(R,result(c1Idx,c2Idx).params);
                                                    
            % Prepare simulation graphics 
            [ax,chain]  = InitializeGraphics(pos,params);
                        
            % Run relaxation steps with dtRelaxation
            for stepIdx = 1:params.numRelaxationSteps
                pos = RCLPolymerStep(pos,R, params,'relaxation');
                UpdateGraphics(pos,stepIdx,params,ax,chain)
            end
            
            % save initial chain position for MSD calculation
            initialPos = pos; 
            
            % Main Simulation loop
            for stepIdx = 1:params.numSteps
                
                % Update polymer position
                pos = RCLPolymerStep(pos,R,params,'simulation');
                
                % Show simulation  
                UpdateGraphics(pos,stepIdx,params,ax,chain) 
                
                % Calculate the variance.!significantly slows down simulations
                if params.calculateVariance
                    for dIdx=1:params.dimension
                        result(c1Idx,c2Idx).monomerPosDiff(:,:,dIdx,rIdx)= (bsxfun(@minus, pos(:,dIdx),pos(:,dIdx)'));
                    end
                end
                
                % Calculate MSD
                if params.calculateMSD% cummulative sum
                    msdTemp(:,stepIdx+1) = (msdTemp(:,stepIdx) *((stepIdx)*params.dt ) + ((sum((pos-initialPos).^2,2))))./((stepIdx+1)*params.dt);
                end
                
                if params.saveChainPosition
                    for mnIdx = 1:params.numBeads
                        % record polymer position to external .txt file
                        fprintf(posfName,'%f,%f,%f\n', pos(mnIdx,1), pos(mnIdx,2),pos(mnIdx,3));
                    end
                end                
                              
            end % simulation steps
            
            if params.calculateMSD
                msd = msd+msdTemp; %accumulate the MSD
            end
            
            % Calculate monomer pairwise distance
            bd                                   = pdist2(pos,pos);
            result(c1Idx,c2Idx).monomerDist(:,:,rIdx) = bd;
            bd(diag(true(1,params.numBeads)))    = Inf; % discard main diagonal
            result(c1Idx,c2Idx).encounterFreqMat = result(c1Idx,c2Idx).encounterFreqMat+(bd<=params.encounterDist);
            result(c1Idx,c2Idx).end2endDist      = (result(c1Idx,c2Idx).end2endDist*(rIdx-1)+bd(1,params.numBeads)^2)/rIdx;
            nnDist                               = nnDist+ std(bd(diag(true(1,params.numBeads-1),1)));% (nnDist*(rIdx-1)+(bd(d1)))/(rIdx);
            cm                                   = mean(pos,1); % chain center of mass
            
            % Save the square radius of gyration
            srog(rIdx)             = sum(sum(bsxfun(@minus, pos,cm).^2,2))/params.numBeads;
            if params.saveChainPosition
                fclose(posfName);
            end
        end % end simulation loop
        
        %-------
        % Start post simulation analysis
        
        % Calculate connectors variance
        if params.calculateVariance
            vDim = zeros(params.numBeads,params.numBeads,params.dimension);
            for dIdx = 1:params.dimension;
                vDim(:,:,dIdx)  = (var(result(c1Idx,c2Idx).monomerPosDiff(:,:,dIdx,:),[],4)');
            end
            result(c1Idx,c2Idx).connectorVariance = sum(vDim,3);
        end
        
        % Save last simulation polymer configuration, used for display
        % in e.g Chimera
        result(c1Idx,c2Idx).lastPolymerConfiguration.chainPos       = pos;
        connectedBeads                                              = [];
        [connectedBeads(:,1),connectedBeads(:,2)]                   = find(triu(R-diag(diag(R,1),1))<0);
        result(c1Idx,c2Idx).lastPolymerConfiguration.connectedBeads = connectedBeads;
        result(c1Idx,c2Idx).nearestNeighborDistSTD                  = (mean(nnDist)/params.numSimulations);
        result(c1Idx,c2Idx).meanSquareRadiusOfGyr                   = sum(srog)/params.numSimulations;% mean square radius of gyration
        
        % Transform the encouner frequency matrix to encounter probability
        for b1Idx = 1:params.numBeads;
            result(c1Idx,c2Idx).encounterProb(b1Idx,:) = result(c1Idx,c2Idx).encounterFreqMat(b1Idx,:)./a.SumIgnoreNaN(result(c1Idx,c2Idx).encounterFreqMat(b1Idx,:));
        end
        
        if params.calculateMSD  % the mean MSD over simulations
            result(c1Idx,c2Idx).msd= msd./params.numSimulations;% mean(msd,3);
            
            %  Fit a model to extract the anomaouls exponent for each monomer
            time      = (0:params.numSteps).*params.dt;
            [anomalousExp,bias] = FitMSD(time, result(c1Idx,c2Idx).msd);
            result(c1Idx,c2Idx).msdFit.monomer.anomalousExp = anomalousExp;
            result(c1Idx,c2Idx).msdFit.monomer.bias         = bias;
            
            % Fit to the mean MSD over monomers
            meanMSD = mean(result(c1Idx,c2Idx).msd,1);
            [meanAnomalousExp, meanBias]= FitMSD(time,meanMSD);
            result(c1Idx,c2Idx).msdFit.mean.anomalousExp = meanAnomalousExp;
            result(c1Idx,c2Idx).msdFit.mean.bias         = meanBias;
        end
        
        if params.fitEncounterCurves
            % fit (1/sum(x^(-beta))*b^-beta to the data
            [fittedBeta,meanBeta, encounterProbOneSide,~] = AnalyseRandomLoopModelSimulationData(result(c1Idx,c2Idx).encounterFreqMat);
            result(c1Idx,c2Idx).encounterProbOneSide      = encounterProbOneSide;
            result(c1Idx,c2Idx).fittedBeta                = fittedBeta;
            result(c1Idx,c2Idx).meanBeta                  = meanBeta;
        end
        
        if params.saveResults
            save(fullfile(params.resultFolder,'result'),'result')
        end
    end
 end
end

function pos = RCLPolymerStep(pos,R,params,state)
% Advance one step in the simulation of the RCL polymer 
% input: 
% pos-  is the previous position,(numMonomers x dimension)
% R- is the Laplacian of the graph representation of the polymer
% (multiplied by string constants)
% params- is system parameters, including time step, friction
% coeffitient and diffusion constant
% state- a flag indication that the system is 'relaxatio' stage or not
% for relaxation stage steps are ran with the dtRlaxation parameters in
% param structure

% output: an updated polymer position 

    if strcmpi(state,'relaxation')
        pos   = pos -R*(pos*params.dtRelaxation) + sqrt(2*params.diffusionConst*params.dtRelaxation).*randn(params.numBeads,params.dimension);
    else
        pos   = pos -R*(pos*params.dt) + sqrt(2*params.diffusionConst*params.dt).*randn(params.numBeads,params.dimension);
    end

end

function R = InitializeConnectivityMatrix(params,matTAD1, matTAD2)
% Create random connections according to the specified connectivity
% percentages in params.connectivityTAD1 or 2
% this function should not be called directly

% input:
% params - ana initialized param struct generated by
% RandomLoopSystemParams.m
% matTAD1, matTAD2- matrix holding the connectivity of TAD1, generated by GenerateRandomConnectivityMatrix

% output:
% R- the connectivity matrix (Laplacian) of the polymer with two TADs 


% Construct the connectivity matrix
R                                              = zeros(params.numBeads);
R(diag(true(1,params.numBeads-1),1))           = -1;  % super diagonal
R(diag(true(1,params.numBeads-1),-1))          = -1;  % sub-diagonal
R(params.beadRange.TAD1,params.beadRange.TAD1) = R(params.beadRange.TAD1,params.beadRange.TAD1)+matTAD1;
R(params.beadRange.TAD2,params.beadRange.TAD2) = R(params.beadRange.TAD2,params.beadRange.TAD2)+matTAD2;

% Construct fixed connectivity Matrix and add it to the
% total
cMat = zeros(size(R));
fc   = params.fixedConnectors;
for cmIdx = 1:size(fc,1);
    cMat(fc(cmIdx,1),fc(cmIdx,2))  = -1;
    cMat(fc(cmIdx,2), fc(cmIdx,1)) = -1;
end

R    = R+cMat;

% Multiply by the spring constant values for heterogeneous polymer
R    = R.*params.springConst;

% Add NN peaks with their spring constant
if ~isempty(params.peakList)
    for pIdx = 1:size(params.peakList,1)
        R(params.peakList(pIdx,1),params.peakList(pIdx,2)) = -1 *params.springConst(params.peakList(pIdx,1),params.peakList(pIdx,2));
        R(params.peakList(pIdx,2),params.peakList(pIdx,1)) = -1 *params.springConst(params.peakList(pIdx,2),params.peakList(pIdx,1));
    end
end

% Sum rows to get the diagonal element value
d    = diag(true(1,params.numBeads));
R(d) = -sum(R,2);

end

function pos = InitializeChainPosition(params)
% Set initial random chain conformation 
% input: param struct generated by RandomLoopSystemParams.m
% output: an numMonomersX dimension matrix of initial polymer position 

pos = zeros(params.numBeads,params.dimension);

for psIdx = 2:params.numBeads
    pos(psIdx,:) = pos(psIdx-1,:)+params.b.*randn(1,params.dimension);
end

if params.dimension ==2
    pos(params.numBeads,3) = 0;
end
end

function cMat = GenerateRandomConnectivityMatrix(numConnectors,numMonomers)
% Generate a realization of the connectivity (Laplacian) of the polymer 
% Input: 
% numConnectors: the total number of added random connectors 
% numMonomers : the total number of monomers in the polymer 
% OutPut: 
% cMat a numMonomers xnumMonomers semi-random laplacian of the graph representatio of the polymer 

possiblePairs = (numMonomers-1)*(numMonomers-2)/2;
z             = zeros(1,possiblePairs);
rp            = randperm(possiblePairs);
rp            = rp(1:numConnectors);
z(rp)         = -1;

% Translate the choices into the matrix
cMat  = zeros(numMonomers);
for mIdx = 1:numMonomers-2
    cMat(mIdx,mIdx+2:end) = z(1:numMonomers-mIdx-1);
    z = z(numMonomers-mIdx:end);
end
 cMat = (cMat+cMat'); % symmetrize 
end

function [ax,chain] = InitializeGraphics(pos,params)
% Initialize graphic objects
% this function is called automatically and should not be called directly. 
% visualiozation only works is display simulation is set to true in
% parameters
% input: pos- and nummonomersX dimension matrix of polymer position 
% output:
ax    = [];
chain = [];
if params.displayChain
    fig   = figure('ToolBar','none');
    cameratoolbar
    figRad = sqrt(params.numBeads)*params.b;
    ax     = axes('XLim',figRad.*[-1 1],'YLim',figRad.*[-1 1],'Parent',fig);
    daspect(ax,[1 1 1])
    chain  = line('XData',pos(:,1),'YData',pos(:,2),'ZData',pos(:,3),'Marker','o','Parent',ax);
end
end

function params = DetermineStepsByRelaxationTime(R,params)
% Determine the number of simulation steps by the longest relaxation time
% of the polymer 
% input: R - connectivity (Laplaciana) matrix, generated for wach
% simulation by InitializeConnectivityMatrix function 
% params- initialized param struct generated by RandomLoopSystemParams.m
% output: updated params structure with the connectivity matrix.


    if params.determineStepsByRelaxationTime
        try
            R                   = R./params.springConst;
            rTemp               = -double(R~=0)+diag(ones(params.numBeads,1));
            rMat                = rTemp-diag(sum(rTemp,2)); % connectivity Matrix
            [~,ggsEigenVals]    = eig(rMat); % eigenvalues of the RCL
            % Find the smallest non-vanishing eigenvalue
            smallestEigenValInd = find(diag(ggsEigenVals)>1e-8,1); % normally it is the second one on the list
            smallestEigenVal    = ggsEigenVals(smallestEigenValInd,smallestEigenValInd);   

            tauk                = params.b^2 /(6*params.diffusionConst*smallestEigenVal);% simulation time
            % split the total relaxation steps into relexation steps with high
            % dt and steps with low dt (to save computation time)
            percRelax                 = params.percentRelaxStep;% the percent of the total time in which high dt is used
            percRun                   = 100-percRelax; % percent of the time low dt is used
            params.numRelaxationSteps = round((percRelax/100)*tauk/params.dtRelaxation) +params.numStepsPastRelaxation; % Added 2000 just to make sure its is relaxed
            params.numSteps           = round((percRun/100)*tauk/params.dt);
        catch
        end
    end
end

function UpdateGraphics(pos,stepIdx,params,ax,chain)
% update polymer positions graphics
% this function should not be called directly
% input:
% pos- an nummonomersX dimension matrix of polymer position 
% stepIdx - simulation step index
% params - initialize param structure generatd by RandomLoopSystemParams.m
% ax - current axes handle
% chain - current chain handle


if params.displayChain
    set(chain,'XData',pos(:,1),'YData',pos(:,2),'ZData',pos(:,3),'parent',ax);
    title(ax,['step ' num2str(stepIdx),' (', params.numSteps,' )'])
    drawnow
end

end

function [anomalousExp,bias] = FitMSD(time, msdMat)
% Fit the MSD curve with a model of the form bias*t^anomalousExp to the
% simulation results 
% input:
% time- a 1d vector of non-negative time points to compute the MSD for 
% msdMat - matrix holding the values of the MSD calculated for the
% simulation 
% output: 
% anomalousExp - the enomalous exponent fitted to simulation data at times
% time
% bias - the bias term in the fit bias*time^anomalousExp


fitModel = fittype('b*x.^a');
fo       = fitoptions('Method', 'NonlinearLeastSquares','Lower',[0,0],...
    'Upper',[1,Inf],...
    'StartPoint',[0.1 0.1],...
    'MaxFunEvals',1000,...
    'MaxIter',600,...
    'TolFun',1e-8,...
    'TolX',1e-8);
anomalousExp = zeros(size(msdMat,1),1);
bias         = zeros(size(msdMat,1),1);
for mIdx = 1:size(msdMat,1)
    sprintf('%s%d\n','Fitting MSD for monomer ', mIdx)
    [fitParams] = fit(time',msdMat(mIdx,:)',fitModel,fo);
    anomalousExp(mIdx) = fitParams.a;
    bias(mIdx)         = fitParams.b;
end

end