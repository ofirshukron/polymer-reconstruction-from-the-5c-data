function [params] = RandomLoopSystemParameters
% Generate parameters for the random loop model based on the analysis of
% the CC data
% simulation will run for each connectivityTAD1 and connectivityTAD2 value
% Two different codes use this parameter file: SimulateRandomLoopModel.m and
% SimnulateRandomLoopModelNonEquillbrium.m
    
params.displayChain       = false;
params.saveResults        = false;
params.simulationType     = 'steadystate'; % type of simulation steadystate| transient
params.saveChainPosition  = false;         % record position to txt file ( recording position considerably slows down simulation about 9 times)
if params.saveChainPosition;params.saveResults= true;end % set to true to have the result struct
params.calculateVariance  = true;    % for debugging purposes  
params.calculateMean      = false;   % calculate the mean of the diffference betwen vectors (connnected monomers)
params.calculateMSD       = false;   % calcualte the MSD and extract the anomalous exponent for all monomers (fitting)
params.resultBaseFolder   = fullfile(pwd,'Results');%'FirstEncounterTime','TwoTADsPeaksAndRandomLoops','AdjustedConnectivity');  % where to save the results (leave black for current working path
params.resultFolderName   = 'Exp1_';
params.trajectoriesFolder = 'Trajectories'; % the name of the folder in which the trajectories are saved. Located in resultFolderName
params.trajectoriesBaseName = 'trajectories'; % the initial name of each trajectory file, will be followed by the experiment indices
params.description        = 'Encounter dist b/20. Simulate connectivity with 5:100 connectors for TAD1 and 0 for TAD2. exclude peaks. A  polymer with 50 monomers.';
params.numBeads           = 307;
params.beadRange.TAD1     = 1:106;      % bead range for TAD D
params.beadRange.TAD2     = 107:307;   % bead range for TAD E
params.ignorePeaks        = true;      % ignore peaks found do not update spring constant (peaks derived from data)
params.fixedConnectors    = [];        % [1 30; 40, 60; 70  90];  % indices of connected monomers in additional to the linear backbone
params.connectivityTAD1   = ([6]).*2./((numel(params.beadRange.TAD1)  -1)*(numel(params.beadRange.TAD1)-2));% fraction of non-NN monomers connected in TAD D (percentage)
params.matchConnectivityOfTAD2ToTAD1 = false; % match the connectivity of TAD2 to that of TAD1
params.connectivityTAD2   = ([10]).*2./((numel(params.beadRange.TAD2) -1)*(numel(params.beadRange.TAD2) -2));          % fraction of non-NN monomers connected in TAD E (precentage)
params.numSimulations     = 1;        % total number of simulations 
params.determineStepsByRelaxationTime = true; % if set to true, the percentRelaxation of step are ran with dtRelaxation
params.percentRelaxStep   = 95;        % [0 100], percent of total steps run with dtRelaxation
params.numSteps           = 1000;      % unused in the case determineStepsByRelaxationTime=true, steps are determined by relaxation time
params.numRelaxationSteps = 1000;      % unused in the case determineStepsByRelaxationTime=true, steps are determined by relaxation time
params.numStepsPastRelaxation = 1000;  % number of steps to simulate after relaxation time still with dtRelaxation. only works when determineStepsByRelaxationTime=True
params.dt                 = 0.01;      % [sec]
params.dtRelaxation       = 0.03;      % dt to use in relaxation steps 
params.dimension          = 3;         % spatial dimension
params.frictionCoef       = 1;         % friction coefficient [Newton][sec]/[mu m]
params.diffusionConst     = 10;        % diffusion constant [mu m]^2 /[sec]
params.MFETForBeads       = [];   % only works when SimulateRandomLoopModelNonEquilibrium is running.  List of beads for which their mean first encounter time is examined. The encounter is made for A meets B before A meets C
params.b                  = sqrt(3);   % 
params.springConst        = (params.dimension.*params.diffusionConst./params.b^2) .*ones(params.numBeads);% 2*pi*params.kB*params.T* params.nnEncounterProb^(2/params.dimension)).*ones(params.numBeads);%./params.dimension;%[Joule]=[Newton][m] 
params.encounterDist      = params.b/20;
params.fitEncounterCurves = false;        % at the end of each simulation fit a curve to the one-sided encounter data
params.experimentalDataFileName = 'savedAnalysisTADDAndE.mat';

% Peak calling for the 5C matrix 
params.numDiagonalToExcludeInPeakCalling = 3; % number of super and subdiagonals to exclude from peak calling
[params.peakList,params.nnSpringConst,params.nnEncounterProb] = FindNearestNeighborsInCCData(params.numDiagonalToExcludeInPeakCalling);% NN spring constant in units of kB*T
if params.ignorePeaks
    params.peakList      = [];
    params.nnSpringConst = []; % spring constant for monomers corresponding to peaks
end

% Set the spring constant for monomer pairs corresponding to peaks of the
% 5C matrix 
n1                       = numel(params.beadRange.TAD1); % numBeads TADD
n2                       = numel(params.beadRange.TAD2); % numBead TADE
params.possiblePairsTAD1 = (n1-2)*(n1-1)/2;              % number of possible pairs TAD D
params.possiblePairsTAD2 = (n2-2)*(n2-1)/2;              % number of possible pairs TAD E

if ~params.ignorePeaks
    % Remove duplicates from the peak list
    ind    = 1;
    inFlag = true;
    while inFlag
        dupInd = (params.peakList(:,2)==params.peakList(ind,1) & params.peakList(:,1) == params.peakList(ind,2));
        params.peakList = params.peakList(~dupInd,:);
        params.nnSpringConst = params.nnSpringConst(~dupInd);
        if ind==size(params.peakList,1)
            inFlag = false;
        else
            ind = ind+1;
        end
    end
    
    % Keep only peaks in the range of the data (numBeads), that means it refers to the first 1..numBeads in the data 
    params.peakList = params.peakList(params.peakList(:,1)<params.numBeads & params.peakList(:,2)<params.numBeads,:);
    
    % Update NN spring const
    for pIdx = 1:size(params.peakList,1)
        params.springConst(params.peakList(pIdx,1),params.peakList(pIdx,2)) = params.kB*params.T*params.nnSpringConst(pIdx);
        params.springConst(params.peakList(pIdx,2),params.peakList(pIdx,1)) = params.kB*params.T*params.nnSpringConst(pIdx);
    end
end

params.encounterModel     = fittype('((x).^(-b))./(sum(x.^(-b)))');
if params.saveResults || params.saveChainPosition
    mkdir(params.resultBaseFolder);% create the folder if doesn't exist
    da                  = clock;
    params.resultFolder = fullfile(params.resultBaseFolder,[params.resultFolderName, sprintf('%d_%d_%d_time_%d_%d',da(3),da(2),da(1),da(4),da(5))]);
    [~,~,~]= mkdir(params.resultFolder);
    if params.saveChainPosition
        % cCeate a directory for trajectories 
        mkdir(fullfile(params.resultFolder,params.trajectoriesFolder));
    end
end

end
