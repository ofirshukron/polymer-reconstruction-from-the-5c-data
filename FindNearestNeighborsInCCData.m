function [nnPairs,harmonicSpringConstNN,nnEncounterProb]= FindNearestNeighborsInCCData(numDiagonals)
% Find peaks corresponding to nearest neighbors in TAD D and E data. 
% peaks are localized by thresholding the image based on the mean nearest
% neighbor probability extracted for the super ans sub diagonal of the
% encounter probability matrix 
% Input: numDiagonals- an integer specifying the number of super and
% sub-diagonals to exclude from peak calling.

% Output:
% nnPairs is a N by 2 list of bead pairs connected (no duplicates)
% harmonicSpringConstantNN is the adjusted spring constant for NN
% nnEncounterProb is the estimated nearest neighbor encounter probability 

% load saved TAD analysis
load('savedAnalysisTADDAndE.mat');

% The saved variable is called 'a'
% numBeads            = a.params.numBeads;
beadRange.bead1 = 1:307;%numBeads;
beadRange.bead2 = 1:307;%numBeads;
dimension       = 3;
numBeads        = beadRange.bead1(end)-beadRange.bead1(1) +1;
% Get encounter frequency matrix 
encounterFreq        = a.encounterMatrix.average(beadRange.bead1, beadRange.bead2); 
% Symmetrize the encounter freq to remove experimental bias in measurement 
encounterFreq        = 0.5*(encounterFreq+encounterFreq');
encounterProb        = zeros(numBeads);
encounterFreqOneSide = zeros(numBeads,numBeads);
encounterProbOneSide = zeros(numBeads,numBeads);
d                    = diag(true(1,size(encounterFreq,1)));% indicator for self interactions of beads

% Transform the encounter frequency matrix to encounter probability matrix
for bIdx = 1:numBeads
    sumRow                              = a.SumIgnoreNaN(encounterFreq(bIdx,:));
    encounterRight                      = nan(1,numBeads);
    encounterLeft                       = nan(1,numBeads);
    encounterRight(1:numel(bIdx:numBeads)) = encounterFreq(bIdx,(bIdx):numBeads); % numbeads-1
    encounterLeft(1:bIdx)                  = encounterFreq(bIdx,(bIdx):-1:1);
   
    v                                   = [encounterLeft;encounterRight];
    v(isnan(v))                         = 0;
    encounterFreqOneSide(bIdx,:)        = sum(v,1);
        
    if sumRow~=0
     encounterProb(bIdx,:)        = encounterFreq(bIdx,:)./sumRow;        % two-sided encounter prob 
     encounterProbOneSide(bIdx,:) = encounterFreqOneSide(bIdx,:)./sumRow; % one-sided encounter prob
    else
        encounterProb(bIdx,:) = 0;
    end
end
 
% Create matrix indicating positions of nearest-neighbors beads of the linear
% chain 

% Get nearest-neighbor encounter probabilites 
nnEncounterFreqAll  = nonzeros(encounterFreq(d)); % the structure of the binned 5C data is such that the diagonal is populated
nnEncounterFreqAll  = nnEncounterFreqAll(~isnan(nnEncounterFreqAll)); % no NANs

nnEncounterProb     = sum(nnEncounterFreqAll)./sum(encounterFreq(:));
encounterProbThresh = encounterProb>nnEncounterProb; % threshold the encounter matrix using the threshold value found

% Symmetrize the peak matrix 
sEncounterProbThresh = encounterProbThresh | encounterProbThresh';

% Ignore the diagonal the sub and super diagonal while finding NN
% Construct an indicator matrix with numDiagonal sub and super diagonal
nDiag = diag(true(1,size(encounterProbThresh,1)),0);
for nIdx = 1:numDiagonals
    nDiag = nDiag | ...
            diag(true(1,size(encounterProbThresh,1)-nIdx),-nIdx)|...
            diag(true(1,size(encounterProbThresh,1)-nIdx),nIdx);
end
           
% Find connected regions in the image 
rp = regionprops(triu(sEncounterProbThresh) &~nDiag,encounterProb,'MaxIntensity','PixelValues','PixelList');
% Create peak list corresponding to Nearest neighbor based on maximal
% probability in a connected region 
peakList = zeros(size(rp,1),2);
for rIdx = 1:size(rp);
    p = rp(rIdx).PixelList(rp(rIdx).PixelValues==rp(rIdx).MaxIntensity,:);
    p = p(1,:); 
    peakList(rIdx,:) = p;
end
if ~isempty(peakList)
% Remove double entries 
ind    = 1;
inFlag = true;
while inFlag
       dupInd = (peakList(:,2)==peakList(ind,1) & peakList(:,1) == peakList(ind,2));       
       peakList = peakList(~dupInd,:);         
       if ind==size(peakList,1)
           inFlag = false;
       else
           ind = ind+1;
       end       
end
end
nnPairs = peakList;

% Calculate the spring constant for each bead pair corresponding to peak
harmonicSpringConstNN = zeros(size(peakList,1),1);
for pIdx = 1:size(peakList,1);
    % take the maximal encounter probability to represent the peak and for
    % the calculation of the associated spring constant
    harmonicSpringConstNN(pIdx) = max([(2*pi)*(encounterProb(peakList(pIdx,1),peakList(pIdx,2)))^(2/dimension),...
                                       (2*pi)*(encounterProb(peakList(pIdx,2),peakList(pIdx,1)))^(2/dimension)]);
end % in units of K_BT
