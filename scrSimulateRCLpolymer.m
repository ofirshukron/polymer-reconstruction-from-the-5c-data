% scrSimulateRCLpolymer

% simulate the Randomly Cross Linked (RCL) polymer for various connectivity levels

close all
curPath = pwd;   
addpath(genpath(pwd));

% Load RCL parameters
[params]        = RandomLoopSystemParameters;

% Run simulations
if strcmpi(params.simulationType,'SteadyState')
    [result,params] = SimulateRandomLoopModel(params);
elseif strcmpi(params.simulationType,'transient')
    [result,params] = SimulateRandomLoopModelNonEquillibrium(params);
else
    error('Unsuppoerted simulaton type')
end
result