function [result,params] = SimulateRandomLoopModelNonEquillibrium(params)
% simulate a polymer with connectors corresponding to peaks of the CC data
% and random loops in each TAD corresponding to the mean beta of the
% experimental CC data. These simulation are intended for exploring non
% equilibrium properties like the MFET between beads and the conditional
% MFET

% load the analyzed experimental data
 load('savedAnalysisTADDAndE.mat');
% Analyze TAD D and E and pass the mean fitted decay exponent to the

 % Parameters
% [params] = RandomLoopSystemParameters;% generate system parameters 
[result] = GenerateRandomLoopResultStruct(params);% generate result structure for all testes connectivity 

n1       = numel(params.beadRange.TAD1);      % numBeads TADD
n2       = numel(params.beadRange.TAD2);      % numBead TADE
d        = diag(true(1,params.numBeads));     % diagonal elements indicator
d1       = diag(true(1,params.numBeads-1),1); % super diagonal element indicator
infMat   = diag(Inf.*ones(1,numel(params.MFETForBeads)),0); % diagonal Inf for the distance matrix used in MFET
% Run Simulation
for c1Idx = 1:numel(params.connectivityTAD1)
    for c2Idx = 1:numel(params.connectivityTAD2)     
         % Preallocations
         s      = 0; % hold the value of radius of gyration
         nnDist = zeros(params.numBeads-1,1);% nearest neighbor square distance
        for rIdx = 1:params.numSimulations
%             tic
             sprintf('Simulation round %d  (%d/%d)\n', rIdx,c1Idx,c2Idx)
            % create chain position txt file
            if params.saveChainPosition
                result(c1Idx,c2Idx).resultFolder    = fullfile(params.resultBaseFolder,sprintf('connectLevelTAD1_%d_connectLevelTAD2_%d',...
                    result(c1Idx,c2Idx).connectivityTAD1(c1Idx),result(c1Idx,c2Idx).connectivityTAD2(c2Idx)));
                mkdir(result(c1Idx,c2Idx).resultFolder);% make folder if doesn't exist already
                posBaseFileName = fullfile(result(c1Idx,c2Idx).resultFolder,sprintf('chainPos_Simulation_%d.txt',rIdx)); % file name to save position of the chain
                posfName        = fopen(posBaseFileName,'wt');% create the file
            end
            
            % Initialize the chain           
            % Create random connections according to the specified percentages
            % Set connectors TAD D
            z     = zeros(1,params.possiblePairsTAD1);
            rp    = randperm(params.possiblePairsTAD1);
            rp    = rp(1:result(c1Idx,c2Idx).numConnectorsTAD1);
            z(rp) = -1; % place -1 for connection
            
            % Translate the z vector of connected choices into the matrix
            matTAD1 = zeros(n1);
            for mIdx = 1:n1-2
                matTAD1(mIdx,mIdx+2:end) = z(1:n1-mIdx-1);
                z = z(n1-mIdx:end);
            end
            matTAD1  = (matTAD1+matTAD1');
%             [connectedBeadsTAD1(:,1) connectedBeadsTAD1(:,2)] = find(triu(matTAD1));
%              connectedBeadsTAD1 = connectedBeadsTAD1+params.beadRange.TAD1(1)-1;
            % Set connectors TAD E
            z     = zeros(1,params.possiblePairsTAD2);
            rp    = randperm(params.possiblePairsTAD2);
            rp    = rp(1:result(c1Idx,c2Idx).numConnectorsTAD2);
            z(rp) = -1;
            
            % Translate the choices into the matrix
            matTAD2 = zeros(n2);
            for mIdx = 1:n2-2
                matTAD2(mIdx,mIdx+2:end) = z(1:n2-mIdx-1);
                z = z(n2-mIdx:end);
            end
            matTAD2 = (matTAD2+matTAD2');
%             [connectedBeadsTAD2(:,1) connectedBeadsTAD2(:,2)] = find(triu(matTAD2));
%             connectedBeadsTAD2 = connectedBeadsTAD2+params.beadRange.TAD1(end);
%             
            % ToDo: exclude update of bead pairs corresponding to peaks 
            % Construct the Rouse matrix
            R                                              = zeros(params.numBeads);
            R(diag(true(1,params.numBeads-1),1))           = -1;
            R(diag(true(1,params.numBeads-1),-1))          = -1;
            R(params.beadRange.TAD1,params.beadRange.TAD1) = R(params.beadRange.TAD1,params.beadRange.TAD1)+matTAD1;
            R(params.beadRange.TAD2,params.beadRange.TAD2) = R(params.beadRange.TAD2,params.beadRange.TAD2)+matTAD2;
            R                                              = R.*params.springConst;
            
            % Add NN peaks with their spring constant
            if ~isempty(params.peakList)
                for pIdx = 1:size(params.peakList,1)
                    R(params.peakList(pIdx,1),params.peakList(pIdx,2)) = -1 *params.springConst(params.peakList(pIdx,1),params.peakList(pIdx,2));
                    R(params.peakList(pIdx,2),params.peakList(pIdx,1)) = -1 *params.springConst(params.peakList(pIdx,2),params.peakList(pIdx,1));
                end
            end
           % sum rows to get the diagonal element value
            R(diag(true(1,params.numBeads),0)) = -sum(R,2);
%             connectedBeads    = [connectedBeadsTAD1;connectedBeadsTAD2];
            
            % find cliques in the non NN connections 
            
            % Initial chain position
            pos = zeros(params.numBeads,params.dimension);
            for psIdx = 2:params.numBeads
                pos(psIdx,:) = pos(psIdx-1,:)+sqrt(params.b^2/params.dimension).*randn(1,params.dimension);
            end
            
            if params.dimension ==2
                pos(params.numBeads,3) = 0;
            end
            
            if params.displayChain
                fig    = figure('ToolBar','none');
                cameratoolbar
                figRad = sqrt(params.numBeads)*params.b;
                ax     = axes('XLim',figRad.*[-1 1],'YLim',figRad.*[-1 1],'Parent',fig);
                daspect(ax,[1 1 1])
                chain  = line('XData',pos(:,1),'YData',pos(:,2),'ZData',pos(:,3),'Marker','o','Parent',ax);
            end            
            
            try
                if params.determineStepsByRelaxationTime
                    rTemp               = -double(R~=0)+diag(ones(params.numBeads,1));
                    rMat                = rTemp-diag(sum(rTemp,2)); % connectivity Matrix
                    [~,ggsEigenVals]    = eig(rMat); % eigenvalues of the GGS
                    % find the smallest non-vanishing eigenvalue
                    smallestEigenValInd = find(diag(ggsEigenVals)>1e-8,1); % normally it is the second one on the list
                    smallestEigenVal    = ggsEigenVals(smallestEigenValInd,smallestEigenValInd);
                    tau0                = params.frictionCoef/min(params.springConst(params.springConst~=0));% characteristic relaxation time
                    tauk                = tau0/smallestEigenVal;% simulation time 
                    % split the total relaxation steps into relexation steps with high
                    % dt and steps with low dt (to save computation time)
                    percRelax                 = params.percentRelaxStep;% the percent of the total time in which high dt is used
                    percRun                   = 100-percRelax; % percent of the time low dt is used
                    params.numRelaxationSteps = (percRelax/100)*tauk/params.dtRelaxation+1000;% add 1000 just to make sure
                    params.numSteps           = (percRun/100)*tauk/params.dt;
                end
                catch
                    % in case of a failure in convergance of the eig
                    % procedure revert to the default num steps specified
                    % in params
                    %
                end
                                    
            % Run relexation steps with dtRelaxation 
            for stepIdx = 1:params.numRelaxationSteps               
                pos   = pos -(1/params.frictionCoef)*R*pos*params.dtRelaxation+ sqrt(2*params.diffusionConst*params.dtRelaxation./params.dimension).*randn(params.numBeads,params.dimension);
                if params.displayChain % plot for debugging purposes
                    set(chain,'XData',pos(:,1),'YData',pos(:,2),'ZData',pos(:,3),'parent',ax);
                    title(ax,['relexation step:', num2str(stepIdx)])
                    drawnow
                end
            end
            
            for stepIdx = 1:params.numSteps               
                pos   = pos -(1/params.frictionCoef)*R*pos*params.dt+ sqrt(2*params.diffusionConst*params.dt./params.dimension).*randn(params.numBeads,params.dimension);
                if params.displayChain % plot for debugging purposes
                    set(chain,'XData',pos(:,1),'YData',pos(:,2),'ZData',pos(:,3),'parent',ax);
                    title(ax,['step:', num2str(stepIdx)])
                    drawnow
                end
            end
                
            
            % Main loop, run until encounter 
            inFlag  = true; 
            stepIdx = 1; % start the count after relexation
            while inFlag
                pos = pos -(1/params.frictionCoef)*R*pos*params.dt + sqrt(2*params.diffusionConst*params.dt).*randn(params.numBeads,params.dimension);
                
                bd  = pdist2(pos(params.MFETForBeads,:),...
                                pos(params.MFETForBeads,:));%,'euc',[],[],[]) +infMat;                
                bd = bd+(params.encounterDist*100).*diag(ones(1,size(bd,1)));% make sure the diagonal is larger than params.encounterDist
                bdInd  = bd<params.encounterDist; % indicator for encounter
                if any(bdInd(1,:))% for A meets B beafore A meets C
                    result(c1Idx,c2Idx).nonEquilibriumProperties.encounterTime(:,:,rIdx) = bdInd.*params.dt*stepIdx;% time in dt units
                    inFlag = false;
                end
                stepIdx = stepIdx+1;
                
                if params.saveChainPosition
                    fprintf(posfName,'%f,%f,%f\n', pos(:,1), pos(:,2),pos(:,3));% record position
                    fprintf(posfName,'\n');% break between time steps
                end
                
                if params.displayChain % plot for debugging purposes
                    set(chain,'XData',pos(:,1),'YData',pos(:,2),'ZData',pos(:,3),'parent',ax);
                    title(ax,num2str(stepIdx))
                    drawnow
                end
              
            end
            
            % Claculate bead distance
            bd           = pdist2(pos,...
                                     pos);
            bd(d)        = Inf; % remove main diagonal
            result(c1Idx,c2Idx).encounterFreqMat  = result(c1Idx,c2Idx).encounterFreqMat+(bd<=params.encounterDist);
            result(c1Idx,c2Idx).end2endDist       = (result(c1Idx,c2Idx).end2endDist*(rIdx-1)+bd(1,params.numBeads)^2)/rIdx;
            nnDist       = nnDist+(bd(d1)).^2;
            cm           = mean(pos,1);
            s            = ((rIdx-1)*s+sum(bsxfun(@minus, pos,cm).^2,2))/rIdx;
            
            if params.saveChainPosition
                fclose(posfName);
            end      
        end
        % save last simulation polymer configuration
        result(c1Idx,c2Idx).lastPolymerConfiguration.chainPos = pos;
        connectedBeads     = [];
        [connectedBeads(:,1),connectedBeads(:,2)] = find(triu(R-diag(diag(R,1),1))<0);
        result(c1Idx,c2Idx).lastPolymerConfiguration.connectedBeads = connectedBeads;
        result(c1Idx,c2Idx).nearestNeighborDistSTD = mean(nnDist)/params.numSimulations;
        result(c1Idx,c2Idx).meanSquareRadiusOfGyr  = sum(s)/params.numBeads; % mean square radius of gyration
        
        % Transform the encouner frequency matrix to encounter probability
        for b1Idx = 1:params.numBeads;
            result(c1Idx,c2Idx).encounterProb(b1Idx,:) = result(c1Idx,c2Idx).encounterFreqMat(b1Idx,:)./a.SumIgnoreNaN(result(c1Idx,c2Idx).encounterFreqMat(b1Idx,:));
        end
        
        % Analysis
        if params.fitEncounterCurves
            % fit (1/sum(x^(-beta))*b^-beta to the data
%             tic
        [fittedBeta,meanBeta, encounterProbOneSide,~] = AnalyseRandomLoopModelSimulationData(result(c1Idx,c2Idx).encounterFreqMat);
        result(c1Idx,c2Idx).encounterProbOneSide      = encounterProbOneSide;
        result(c1Idx,c2Idx).fittedBeta                = fittedBeta;
        result(c1Idx,c2Idx).meanBeta                  = meanBeta;

        end                        
        
       if params.saveResults
          save(fullfile(params.resultFolder,'result'),'result')
       end
        
       
    end
end


