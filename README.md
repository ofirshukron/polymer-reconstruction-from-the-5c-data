### Reconstruction of a randomly cross-linked polymer model from chromosomal capture data ###

This user manual contains detailed explanation for the structure of the software package to perform simulations of Randomly Cross-Linked (RCL) polymer. 
The simulation framework was used to generate the results reported in 
>Shukron, Ofir, and David Holcman. "Transient chromatin properties revealed by polymer models and stochastic simulations constructed from Chromosomal Capture data." PLoS computational biology 13.4 (2017): e1005469.

The article is available [here](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005469) and on the bionewmetrics [website](http://bionewmetrics.org/reconstruction-of-a-polymer-model-from-the-chromatin-capture-data/).

# other resources #
For information about the construction and properties of the randomly cross-linked polymer used in this project, please see our [project page](https://bitbucket.org/ofirshukron/randomlycrosslinkedpolymer/wiki/Home) on Bitbucket, our [project page](http://bionewmetrics.org/two-loci-single-particle-trajectories-analysis-constructing-a-first-passage-time-statistics-of-local-chromatin-exploration/) in the Bionewmetrics website, and the supporting [article](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.96.012503). 



### How do I get set up? ###
Please consult the [Wiki page](https://bitbucket.org/ofirshukron/polymer-reconstruction-from-the-5c-data/wiki/Home) for further details of the simulation framework and the data provided.





