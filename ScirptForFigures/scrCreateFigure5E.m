% scrGenerate figure 5E
% the MSD of monomers 26,54,87 in TAD D+E and TAD D alone 
% start from the folder this code is in it
% load results 

%% MSD for TAD D+E

resultFolder = fullfile(pwd,'..','..','..','..','..','OwnCloud','RCLpolymerResults','AnomalousExp','Exp03_13_9_2016_time_21_41');
load(fullfile(resultFolder,'result.mat'));
time  = (1:result(1).params.numSteps+1).*result(1).params.dt;
m26   = result(1,1).anomalousExp(26,:);
m64   = result(1,1).anomalousExp(64,:);
m87   = result(1,1).anomalousExp(87,:); 

% Fit parameters
a26 = 0.028; b26 = 0.3;
a64 = 0.034; b64 = 0.22;
a87 = 0.061; b87 = 0.35;

% Curve colors 
c    = parula(20); 
fig1 = figure('Rendere','painters');
title('TAD D+E')
ax1  = axes('Parent',fig1,'NextPlot','Add');
line('XData',time,'YData', m26,'Parent',ax1,'LineWidth',4,'DisplayName','MSD m_{26}','Color', c(17,:));
line('XData',time,'YData',a26*time.^b26,'Parent',ax1,'LineWidth',4,'LineStyle','--','Color',c(17,:),'DisplayName',[num2str(a26) 't' '^{' num2str(b26),'}']);
line('XData',time,'YData', m64,'Parent',ax1,'LineWidth',4,'DisplayName','MSD m_{64}','Color',c(12,:));
line('XData',time,'YData',a64*time.^b64,'Parent',ax1,'LineWidth',4,'LineStyle','--','Color',c(12,:),'DisplayName',[num2str(a64) 't' '^{' num2str(b64),'}']);
line('XData',time,'YData', m87,'Parent',ax1,'LineWidth',4,'DisplayName','MSD m_{87}','Color',c(3,:));
line('XData',time,'YData',a87*time.^b87,'Parent',ax1,'LineWidth',4,'LineStyle','--','Color',c(3,:),'DisplayName',[num2str(a87) 't' '^{' num2str(b87),'}']);
xlabel('Time [sec]');
ylabel('MSD');
set(ax1,'Box','off','LineWidth',3,'FontSize',30,'XLim',[0 31])
leg1 = legend(flipud(get(ax1,'Children')));
set(leg1,'Box','off')


%% plot MSD for the TAD D alone 

resultFolder = fullfile(pwd,'..','..','..','..','..','OwnCloud','RCLpolymerResults','AnomalousExp','Exp03_13_9_2016_time_21_41');
load(fullfile(resultFolder,'result.mat'));
time  = (1:result(1).params.numSteps+1).*result(1).params.dt;
m26   = result(1,1).anomalousExp(26,:);
m64   = result(1,1).anomalousExp(64,:);
m87   = result(1,1).anomalousExp(87,:); 

% Fit parameters
a26 = 0.028; b26 = 0.3;
a64 = 0.034; b64 = 0.22;
a87 = 0.061; b87 = 0.35;

% Curve colors 
c    = parula(20); 
fig1 = figure('Rendere','painters');
title('TAD D')
ax1  = axes('Parent',fig1,'NextPlot','Add');
line('XData',time,'YData', m26,'Parent',ax1,'LineWidth',4,'DisplayName','MSD m_{26}','Color', c(17,:));
line('XData',time,'YData',a26*time.^b26,'Parent',ax1,'LineWidth',4,'LineStyle','--','Color',c(17,:),'DisplayName',[num2str(a26) 't' '^{' num2str(b26),'}']);
line('XData',time,'YData', m64,'Parent',ax1,'LineWidth',4,'DisplayName','MSD m_{64}','Color',c(12,:));
line('XData',time,'YData',a64*time.^b64,'Parent',ax1,'LineWidth',4,'LineStyle','--','Color',c(12,:),'DisplayName',[num2str(a64) 't' '^{' num2str(b64),'}']);
line('XData',time,'YData', m87,'Parent',ax1,'LineWidth',4,'DisplayName','MSD m_{87}','Color',c(3,:));
line('XData',time,'YData',a87*time.^b87,'Parent',ax1,'LineWidth',4,'LineStyle','--','Color',c(3,:),'DisplayName',[num2str(a87) 't' '^{' num2str(b87),'}']);
xlabel('Time [sec]')
ylabel('MSD')
set(ax1,'Box','off','LineWidth',3,'FontSize',30,'XLim',[0 31])
leg1 = legend(flipud(get(ax1,'Children')));
set(leg1,'Box','off')