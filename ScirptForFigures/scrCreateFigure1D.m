% scrCreateFigure1D

% load experimental data analysis 
% make sure the working directory is the one containing this script

load('savedAnalysisTADDAndE.mat');
a.CreateEncounterFrequencyMatrices
[fittedBetaExp,meanBeta, encounterProbOneSide,encounterFreqOneSide,fittedBias,gof] =...
 AnalyseRandomLoopModelSimulationData(a.encounterMatrix.average);
load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','ExperimentalData','Fitting','fittedEncounterExpData.mat'))
fontSize = 50;
fontName = 'Arial';

figure1D = figure('Name','fittedBeta','Color','w');
% The saved variable is called 'a'
fittedBeta = a.results.fit.average.exp;

% Create axes
axes1D = axes('Parent',figure1D,'LineWidth',5,'FontSize',fontSize,'YLim',[0 1.5],'XLim',[0 307],...
    'FontName',fontName);
axPos = get(axes1D,'Position');
hold(axes1D,'all');
for fIdx = 1:numel(fittedBetaExp)
plot(axes1D,fIdx,fittedBetaExp(fIdx),'Marker','o',...
    'MarkerFaceColor','b',...
    'LineStyle','none',...
    'DisplayName',['Bead ' num2str( fIdx)],...
    'Color','b','MarkerSize',8,...
    'LineWidth',4,'Parent',axes1D,'HandleVisibility','off')
end

lD    = annotation('line',[axPos(1) axPos(1)+axPos(3)*106/307],[axPos(2)+axPos(4)*0.95 axPos(2)+axPos(4)*0.95],'LineWidth',5);
lE    = annotation('line',[axPos(1)+axPos(3)*106/307 axPos(1)+axPos(3)],[axPos(2)+axPos(4)*0.97 axPos(2)+axPos(4)*0.97],'LineWidth',5);
poslD = get(lD,'Position');
poslE = get(lE,'Position');
annotation('textbox',[poslD(1)+(poslD(3))/2 poslD(2)*0.99 0.1 0.1],'LineStyle','none','string','D','FontSize',fontSize,'Fontname',fontName)% TAD D annotation 
annotation('textbox',[poslE(1)+(poslE(3))/2 poslD(2)*0.99 0.1 0.1],'LineStyle','none','string','E','FontSize',fontSize,'Fontname',fontName)% TAD E annotation 

% plot mean values 
fD    = fittedBetaExp(1:106);
fE    = fittedBetaExp(107:307);
meanD = mean(fD(~isnan(fD)));
meanE = mean(fE(~isnan(fE)));
line('XData',[1 106],'YData',[meanD meanD],'Parent',axes1D,'LineWidth',5,'LineStyle','-.','DisplayName',['mean \beta_D =', num2str(meanD)])% mean value TAD D
line('XData',[107 307],'YData',[meanE meanE],'Parent',axes1D,'LineWidth',5,'LineStyle','--','DisplayName',['mean \beta_E=' num2str(meanE)]) % mean values TAD E

% add axes labels
xlabel('Bead position','FontSize',fontSize,'FontName',fontName,'Parent',axes1D)
ylabel('Decay \beta Exponent','Parent',axes1D,'FontSize',fontSize,'FontName',fontName)
% sp = spaps(1:307,fittedBeta,7);
% hold on, fnplt(sp)


% add markers for beta corresponding to specific and non specific long
% range interactions 
[nnPairs,~,~]= FindNearestNeighborsInCCData(3);
beadsToPlot = [24 100, 162];
for nIdx = 1:numel(beadsToPlot)
    plot(beadsToPlot(nIdx),fittedBetaExp(beadsToPlot(nIdx)),'sr','MarkerSize',18,...
        'MarkerEdgeColor','r','MarkerFaceColor','r','HandleVisibility','off')
%     plot(nnPairs(nIdx,2),fittedBetaExp(nnPairs(nIdx,2)),'or','MarkerSize',6,'MarkerEdgeColor','r','MarkerFaceColor','r')
end

leg1D = legend(get(axes1D,'Children'));
set(leg1D,'Box','off')