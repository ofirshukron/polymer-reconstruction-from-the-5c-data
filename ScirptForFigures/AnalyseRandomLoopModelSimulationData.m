function [fittedBeta,meanBeta, encounterProbOneSide,encounterFreqOneSide,fittedBias,gof] = AnalyseRandomLoopModelSimulationData(encounterFreqMat,encounterModel)
% Analysis of encounter frequency matrix from simulation of the
% random loop model
% input: encounterFreqMat is a square NxN matrix of encounter frequencies
% encounterModel is a model of type fittype which specifies by which model
% to fit the encounter probability curves and give mean Beta

% First make sure that all nan values are 0
encounterFreqMat(isnan(encounterFreqMat))= 0;

% set default encounter probability model if not supplied
if ~exist('encounterModel','var')
%         encounterModel = fittype('(x.^(-b))./sum(x.^(-b))');
    encounterModel = fittype('a.*(x.^(-b))'); % make proper changes below
end
numBeads             = size(encounterFreqMat,1);

% Preallocations
fittedBeta           = zeros(1,numBeads);% decay exp. values
fittedBias           = zeros(1,numBeads);% bias values
encounterProbOneSide = nan(numBeads,numBeads-1);
fittedEncounter      = zeros(numBeads,numBeads-1);
encounterFreqOneSide = nan(numBeads,numBeads-1);

% fit (1/sum(x^(-beta))*b^-beta to the data
for bIdx = 1:numBeads
    if sum(encounterFreqMat(bIdx,:)~=0)>2 % if the number of valid entries is more than two
        sprintf('fitting monomer %d\n',bIdx)
        encounterRight                           = zeros(1,numBeads-1);
        encounterLeft                            = zeros(1,numBeads-1);
        encounterRight(1:numel(bIdx+1:numBeads)) = encounterFreqMat(bIdx,bIdx+1:(numBeads)); % numbeads-1
        encounterLeft(1:bIdx-1)                  = encounterFreqMat(bIdx,bIdx-1:-1:1);
        v                                        = [encounterLeft;encounterRight];
        encounterFreqOneSide(bIdx,:)             = sum(v,1);% % sum contributions from both sides
    
        rowSum                       = sum(encounterFreqOneSide(bIdx,~isnan(encounterFreqOneSide(bIdx,:))));    
        encounterProbOneSide(bIdx,:) = encounterFreqOneSide(bIdx,:);%./rowSum;        
        includedPlaces = find(sum(v,1)~=0);
        % fit
        [fitCoef,gof(bIdx)]          = fit((includedPlaces)',encounterProbOneSide(bIdx,includedPlaces)',encounterModel,...
                'Lower',[0 0],'Upper',[1e6 2] ,'StartPoint',[1e5 1],...
                'TolFun',1e-14,'TolX',1e-14,...
                'MaxFunEvals',2000,'MaxIter',2000,...
                'DiffMinChange',1e-14,'DiffMaxChange',0.001);% fit
            fittedBeta(bIdx)             = fitCoef.b;% save decay beta exp            
            fittedBias(bIdx)             = fitCoef.a; % save bias value            
%             fittedEncounter(bIdx,:)      = encounterModel(fittedBias(bIdx),fittedBeta(bIdx),(1:numBeads-1));
            fittedEncounter(bIdx,:)      = encounterModel(fittedBias(bIdx),fittedBeta(bIdx),(1:numBeads-1));
    else
        % no data points
        fittedBias(bIdx)             = NaN;
        fittedBeta(bIdx)             = NaN;
        fittedEncounter(bIdx,:)      = NaN;
    end
end
meanBeta = mean(fittedBeta(~isnan(fittedBeta)));% ignore NAN values
