%scrCreateSupFig2B
% measure of similarity between the experimental EP matrix and the
% simulation EP matrix 

% Make sure you're in the code folder!

close all 
load('savedAnalysisTADDAndE.mat');% variable called 'a'

% Peaks and random loops 
peaksAndRandomLoopsResult = fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksAndRandomLoops','resultsTwoTADsPeaksAndRandomLoops.mat');
peaksNoRandomLoopsResult  = fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksNoLoops','resultsTwoTADsPeaksNoLoops.mat');
% load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksAndRandomLoops','Exp31_29_4_2016_time_10_45','result.mat'))

% Peaks + random loops 
load(peaksAndRandomLoopsResult);

% Peaks no random loops 
load(peaksNoRandomLoopsResult);

simEFMatrix = zeros(307);
for rIdx = 1:numel(r)
    simEFMatrix = simEFMatrix+ r(rIdx).encounterFreqMat;
end
simEPMatrix = zeros(307);
for bIdx = 1:307;
    simEPMatrix(bIdx,:) = simEFMatrix(bIdx,:)./sum(simEFMatrix(bIdx,:));
end

fontSize     = 50;
lineWidth    = 4;
xLim         = [1 307];
yLim         = [0 1];
bRange.bead1 = 1:307;
bRange.bead2 = 1:307;
[osExpEPMatrix,ts,expEPMatrix] = a.GetEncounterProbabilityMatrix(bRange,'Average');
% average the rows
for bIdx = 1:307
    osExpEPMatrix(bIdx,:) = osExpEPMatrix(bIdx,:)./a.SumIgnoreNaN(osExpEPMatrix(bIdx,:));
end

% Divide by the number of valid rows 
numValidRows  = 285; 
osSimAverage  = a.SumIgnoreNaN(osExpEPMatrix)./numValidRows;
% Calculate the CDF
expAverageCDF = cumsum(osSimAverage);

% Show the cdf of the exp encounter prob. matrix 
% Experimental data 
fig1 = figure('FileName','expAverageCDF');
ax1  = axes('Parent',fig1,'FontSize',fontSize,'LineWidth',lineWidth,'Box','off','NextPlot','Add');

plot(ax1,1:306, expAverageCDF ,'LineWidth',lineWidth)
xlabel(ax1,'Distance [monomer]','fontSize',fontSize);
ylabel(ax1,'CDF','fontSize',fontSize);
% zlabel(ax1,'Encounter prob.','fontSize',fontSize);
set(ax1,'XLim',xLim,'YLim',yLim,'LineWidth',lineWidth,'FontSize',fontSize);

% Create the one sided siulation encounter prob matrix 
simEFMatrix = zeros(307);
for rIdx = 1:numel(r)
    simEFMatrix = simEFMatrix+ r(rIdx).encounterFreqMat;
end
simEPMatrix = zeros(307);
for bIdx = 1:307;
    simEPMatrix(bIdx,:) = simEFMatrix(bIdx,:)./sum(simEFMatrix(bIdx,:));
end

osSimEFMatrix = zeros(307,306);
osSimEPMatrix = zeros(307,306);
for bIdx = 1:307
    leftEncounters        = [fliplr(simEFMatrix(bIdx,1:(bIdx-1))), zeros(1,307-bIdx)];
    rightEncounters       = [simEFMatrix(bIdx,bIdx+1:end),zeros(1,bIdx-1)];
    numZeros              = [leftEncounters~=0 ;rightEncounters~=0];
    sZ                    = sum(numZeros);
    osSimEFMatrix(bIdx,:) = (leftEncounters+rightEncounters);
    osSimEPMatrix(bIdx,:) = osSimEFMatrix(bIdx,:)./a.SumIgnoreNaN(osSimEFMatrix(bIdx,:));
end

numValidRows  = 307; 
osSimAverage  = a.SumIgnoreNaN(osSimEPMatrix)./numValidRows;
% Calculate the CDF
simAverageCDF = cumsum(osSimAverage);
plot(ax1,1:306,simAverageCDF,'LineWidth',lineWidth)

% Calculate the maximal measure between the CDFs 

Dmax = max(abs(simAverageCDF-expAverageCDF));
sprintf('%s%f', 'D_max value for model with peaks and random loops:' , Dmax)
    

% Perform the same analysis when the averaging is performed with no regards
% to monomer distances 
expAverageCDFnd = cumsum(a.SumIgnoreNaN(expEPMatrix))./285;
simAverageCDFnd = cumsum(a.SumIgnoreNaN(simEPMatrix))./307;
fig2            = figure('FileName','ExpVsSimAverageCDFnoDistances');
ax2             = axes('Parent',fig2,'NextPlot','Add');
plot(ax2,1:307,expAverageCDFnd,'LineWidth',lineWidth);
plot(ax2,1:307,simAverageCDFnd,'LineWidth',lineWidth);
xlabel(ax2,'Distance [monomer]','fontSize',fontSize);
ylabel(ax2,'CDF','fontSize',fontSize);
set(ax2,'XLim',xLim,'YLim',yLim,'LineWidth',lineWidth,'FontSize',fontSize);
[Dmaxnd,maxInd] = max(abs(expAverageCDFnd-simAverageCDFnd));
% mark the position of the maximum on the two curves 
plot(ax2,maxInd,expAverageCDFnd(maxInd),'o','markerFaceColor','k','markerEdgeColor','none','LineWidth',lineWidth)
plot(ax2,maxInd,simAverageCDFnd(maxInd),'o','markerFaceColor','k','markerEdgeColor','none','LineWidth',lineWidth)


