% This script creates figure 3B of the article: comparison of the fitted beta of model with
% peaks to the fit of the experimental beta signal data

% Make sure you're polymerChaindynamic code folder!

 
% [fittedBetaExp] = AnalyseRandomLoopModelSimulationData(a.encounterMatrix.average);
% D:\Ofir\ENS\OwnCloud\savedResults\TwoTADsPeaksNoLoops\Exp10_1_5_2016_time_19_10

load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksNoLoops','resultsTwoTADsPeaksNoLoops.mat'))
load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','ExperimentalData','Fitting','fittedEncounterExpData.mat'))

% load(fullfile(pwd,'..','..','OwnCloud','savedResults','TwoTADsPeaksNoLoops','Exp10_1_5_2016_time_19_10','result.mat'))
% the saved variable is called result
% [fittedBetaPeaks,meanBeta, encounterProbOneSide,encounterFreqOneSide,fittedBias,gof] =...
% AnalyseRandomLoopModelSimulationData(result.encounterFreqMat);


% Collect simulation results

fittedBetaSim = zeros(numel(r),307);
for rIdx = 1:numel(r)
    fittedBetaSim(rIdx,:)= r(rIdx).fittedBeta;
end

lowerBound = min(fittedBetaSim);
upperBound = max(fittedBetaSim);
meanFittedBetaSim = mean(fittedBetaSim);

fontSize   = 50;
fontName   = 'Arial';
lineWidth  = 3; 
fig4D      = figure; 
ax4D       = axes('Parent',fig4D,'FontSize',fontSize,'FontName',fontName,...
                  'LineWidth',lineWidth,'NextPlot','Add');
patch([1:307, 307:-1:1],[upperBound, fliplr(lowerBound)],'b','FaceAlpha',0.3,'EdgeColor','none','DisplayName','Error bounds')
plot(ax4D,1:307,fittedBetaExp,'LineWidth',lineWidth,'DisplayName','Experimental data'),
plot(ax4D, 1:307,meanFittedBetaSim,'LineWidth',lineWidth,'DisplayName','Simulation'),
xlabel(ax4D, 'Monomer position');
ylabel(ax4D, 'Decay exponent')
set(ax4D,'XLim',[1 307])
l =legend(get(ax4D,'Children'));
set(l,'Box','off')

% calculate the difference between the mean and the experimental fitted
% beta 
inds   = ~isnan(fittedBetaExp) & fittedBetaExp>0.1 & fittedBetaExp<1.5;% truncate values 
msefit = norm((meanFittedBetaSim(inds)-fittedBetaExp(inds)),1)/norm(fittedBetaExp(inds),1)



% 
% meanBetaD = mean(fittedBetaPeaks(1:106));
% meanBetaE = mean(fittedBetaPeaks(107:307));
% 
% fontSize  = 50;
% fontName  = 'Arial';
% lineWidth = 3; 
% 
% fig3C  = figure;
% axes3C = axes('Parent',fig3C,'FontSize',fontSize,'FontName',fontName,...
%               'LineWidth',lineWidth,'NextPlot','Add',...
%               'XLim',[0 307],'YLim',[0 1.5],'Box','off',...
%               'Units','norm');
% plot(axes3C,1:307,fittedBetaExp,'DisplayName','Experimental data','LineWidth',lineWidth,'Color','r')
% plot(axes3C,1:307,fittedBetaPeaks,'DisplayName','Simulations','LineWidth',lineWidth,'Color','b')
% % add mean values 
% plot(axes3C,[1 106],[meanBetaD meanBetaD],'LineStyle','--','LineWidth',lineWidth+1,'HandleVisibility','off')
% plot(axes3C,[107 307],[meanBetaE meanBetaE],'DisplayName',['mean \beta_E=',num2str(meanBetaE)],'LineStyle','-.','LineWidth',lineWidth+1,'HandleVisibility','off')
% % add annotation next to the mean beta lines 
% annotation('textbox',[0.2,0.8,0.1 0.1],...
%            'String',['mean \beta_D=',num2str(meanBetaD)],...
%            'FontSize',fontSize,...
%            'FontName',fontName,...
%            'LineStyle','none')
% annotation('textbox',[0.55,0.8,0.1 0.1],...
%            'String',['mean \beta_E=',num2str(meanBetaE)],...
%            'FontSize',fontSize,...
%            'FontName',fontName,...
%            'LineStyle','none')       
%        
% line('XData',[1 106],'YData',[1.5 1.5],'LineWidth',3,'Parent',axes3C)
% line('XData',[107 307],'YData',[1.45 1.45],'LineWidth',3,'Parent',axes3C)
% annotation('textbox',[0.2,0.9,0.1 0.1],...
%            'String','D',...
%            'FontSize',fontSize,...
%            'FontName',fontName,...
%            'LineStyle','none')       
% annotation('textbox',[0.6,0.9,0.1 0.1],...
%            'String','E',...
%            'FontSize',fontSize,...
%            'FontName',fontName,...
%            'LineStyle','none')       
% 
% % insert legend
% leg3C = legend(flipud(get(axes3C,'Children')));
% set(leg3C,'FontSize',fontSize,'FontName',fontName,'Box','off','Position',[0.22 0.19 0.3 0.16])
% % insert labels
% xlabel(axes3C,'Monomer position','FontSize',fontSize,'FontName',fontName)
% ylabel(axes3C,'Decay \beta exponent','FontSize',fontSize,'FontName',fontName)
