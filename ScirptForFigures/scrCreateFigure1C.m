% scriprt to create figure 1C 
% Show all one-sided encounter probabilities and a fit to their average 

load('savedAnalysisTADDAndE.mat');% load analyzed data
% create the encounter frequency matrix 
a.CreateEncounterFrequencyMatrices
% Create figure 1C, one sided encounter probability
[fittedBetaExp,meanBeta, encounterProbOneSide,encounterFreqOneSide,fittedBias,gof] =...
 AnalyseRandomLoopModelSimulationData(a.encounterMatrix.average);
fontSize = 50;
fontName = 'Arial';
fig2 = figure('Units','norm','Name','meanDataFitTADDAndEexperimentalData');
ax2  = axes('Parent',fig2,'Units','norm',...
    'Box','off','NextPlot','Add','FontSize',fontSize,...
    'fontName',fontName,...
    'XLim',[0 307],...
    'YLim',[0 0.31]);
plot(ax2,1:306,encounterProbOneSide','LineWidth',3,'HandleVisibility','off')

meanBias = 1/sum((1:306).^(-meanBeta)); 
plot(1:306,(1/sum((1:306).^(-meanBeta))).*(1:306).^(-meanBeta),...
    'k','Linewidth',5,...
    'LineStyle','-.',...
    'DisplayName','P_E =0.08/Distance^{0.77}')
xlabel(ax2,'Distance [monomer]','FontSize',fontSize,'fontName',fontName)
ylabel(ax2,'Encounter probability','FontSize',fontSize,'fontName',fontName)
leg2 = legend(get(ax2,'Children'));
set(leg2,'Box','off','FontSize',fontSize,'fscrCreateFigure1ContName',fontName)