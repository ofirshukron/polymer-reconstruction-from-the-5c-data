%scrCreateFigure5D
% a script to create figure 5B fitting an exponential to the conditional
% mean first encouner time between 3 monomers in a chain of 307 monomers
% (TAD D and E) 

% Load results, make sure the working path is the one containing this
% script 
% D:\Ofir\ENS\OwnCloud\savedResults\ConditionalEncounterTime\OneTADPeaksAndRandomLoops\Exp07_18_5_2016_time_15_48
resultFilePath = fullfile(pwd,'..','..','..','..','..','OwnCloud','savedResults','ConditionalEncounterTime','OneTADPeaksAndRandomLoops','Exp07_18_5_2016_time_15_48','result.mat');
load(resultFilePath);
eTimes        = result.nonEquilibriumProperties.encounterTime;
beads         = result.nonEquilibriumProperties.beads;
eTimes      = eTimes(:,:,1:10000);% take half to have it comparable to two TADs simulation 
et12          = [eTimes(1,2,:)];
et12          = nonzeros(et12(:));
[h12, bins12] = hist(et12,50);

et13          = [eTimes(1,3,:)];
et13          = nonzeros(et13(:));
[h13, bins13] = hist(et13,50);

% fit a single exponential model to the conditional encounter time of bead
% 1 meeting bead 2 befor bead 1 meets bead 3
fType = fittype('a*exp(b*x)');
fOpt  = fitoptions(fType);
set(fOpt,'Lower',[0 -1],'Upper',[1e4 0],'StartPoint',[1000,-0.005],...
         'Algorithm','Trust-Region','DiffMinChange',1e-12,'DiffMaxChange',0.2,...
         'TolFun',1e-12,'TolX',1e-12)

% fit conditional encounter data
fit12 = fit(bins12',h12',fType,fOpt);
fit13 = fit(bins13',h13',fType,fOpt);

% plot the curve fitting for the conditional encounter bead 1 bead 2 before
% bead 1 bead 3
fig12 = figure; 
ax12  = axes('Parent',fig12,'NextPlot','Add','XLim',[0 bins12(end)],...
             'FontSize',50,...
             'LineWidth',3);
axis tight
p     = parula(10);
line('XData',bins12,'YData',h12,'Marker','o','MarkerSize',12,...
     'MarkerFaceColor','k',...
     'MarkerEdgeColor',p(5,:),...
     'DisplayName',['Encounter m_{',num2str(beads(1)), '} m_{',num2str(beads(2)),'}'],...
     'LineStyle','none','Parent', ax12);
 % plot fit 1-> 2 
line('XData',[0 bins12],'YData',fit12([0 bins12]),'Marker','none',...
     'Color','b',...
     'LineWidth',4,...
     'DisplayName',['Fit m_{',num2str(beads(1)), '} m_{',num2str(beads(2)),'}'],...    
     'Parent', ax12)
xlabel('Time [sec]','FontSize',50,'Parent',ax12)

% Add legend 
leg12 = legend(flipud(get(ax12,'Children')));
set(leg12,'Box','off');
% Add annotations
str12 = [num2str(fit12.a),'e^{', num2str(fit12.b),'}'];
dim   = [.2 .3 .3 .3];
annotation('textbox',dim,'String',str12,'FitBoxToText','on','FontName','Arial','FontSize',50,'LineStyle','none');

% plot fit curve for the conditional encounter probability bead 1-> bead 3
% before bead1 -> bead 2
fig13 = figure; 
ax13  = axes('Parent',fig13,'NextPlot','Add','XLim',[0 bins13(end)],...
    'FontSize',50,'LineWidth',4,'Units','norm','FontName','Arial');
axis tight
p     = parula(10);
line('XData',bins13,'YData',h13,'Marker','o','MarkerSize',12,...     
     'MarkerFaceColor','k',...
     'MarkerEdgeColor',p(4,:),...
     'DisplayName',['Encounter m_{',num2str(beads(1)), '} m_{',num2str(beads(3)),'}'],...
     'LineStyle','none','Parent', ax13);
 
line('XData',[0 bins13],'YData',fit12([0 bins13]),'Marker','none',...
     'Color','b',...
     'LineWidth',4,...
     'DisplayName',['Fit m_{',num2str(beads(1)), '} m_{',num2str(beads(3)),'}'],...    
     'Parent', ax13)
xlabel('Time [sec]','FontSize',50,'Parent',ax13,'FontName','Arial')
% add legend
leg13 = legend(flipud(get(ax13,'Children')));
set(leg13,'Box','off');
% add annotation 
dim = [.2 .3 .3 .3];
format short
str13 = [num2str(fit13.a),'e^{', num2str(fit13.b),'}'];
annotation('textbox',dim,'String',str13,'FitBoxToText','on','FontName','Arial','FontSize',50,'LineStyle','none');