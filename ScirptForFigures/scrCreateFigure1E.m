% scr create Figure 1E

load('savedAnalysisTADDAndE.mat');% load analyzed data
% create the encounter frequency matrix 
a.CreateEncounterFrequencyMatrices
% Create figure 1C, one sided encounter probability
[fittedBetaExp,meanBeta, encounterProbOneSide,encounterFreqOneSide,fittedBias,gof] =...
 AnalyseRandomLoopModelSimulationData(a.encounterMatrix.average);
fontSize = 50;
fontName = 'Arial';
lineWidth = 4;

format short
beadsToPlot = [24 , 100, 162];
fig1E  = figure;
axes1E = axes('Parent',fig1E,'FontSize',fontSize,'FontName',fontName,...
    'Box','off','XLim',[0 306],'NextPlot','Add','LineWidth',3);
hold on
fitColor = hot(numel(beadsToPlot));
lineColor = parula(numel(beadsToPlot));
for bIdx = 1:numel(beadsToPlot)
plot(axes1E,1:306,encounterProbOneSide(beadsToPlot(bIdx),:),'LineWidth',lineWidth,...
     'DisplayName',['P_{',num2str(beadsToPlot(bIdx)),'}']);
end

for bIdx = 1:numel(beadsToPlot)
plot(axes1E,1:306,(1./sum((1:306).^(-fittedBeta(beadsToPlot(bIdx))))).*(1:306).^(-fittedBeta(beadsToPlot(bIdx))),...
     'LineWidth',3,'LineStyle','-.',...
     'DisplayName',['\beta_{',num2str(beadsToPlot(bIdx)),'}=',num2str(fittedBeta(beadsToPlot(bIdx)))])
end
leg1E = legend(flipud(get(axes1E,'Children')));
set(leg1E,'FontSize',fontSize,'FontName',fontName,'Box','off');
xlabel(axes1E,'Distance [monomer]','FontSize',fontSize,'FontName',fontName)
ylabel(axes1E,'Encounter probability','FontSize',fontSize,'FontName',fontName)
