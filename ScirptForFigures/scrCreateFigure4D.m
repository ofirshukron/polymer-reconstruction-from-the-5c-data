% script to creat figure 4D, comparison of the fitted beta of model with
% peaks and random loops to the fit of the experimental data

% Make sure you're  in the code folder!

% % load experimental data
% load('savedAnalysisTADDAndE.mat');% load analyzed data
% % create the encounter frequency matrix 
% a.CreateEncounterFrequencyMatrices
% 
% [fittedBetaExp,meanBeta, encounterProbOneSide,encounterFreqOneSide,fittedBias,gof] =...
%  AnalyseRandomLoopModelSimulationData(a.encounterMatrix.average);

load('savedAnalysisTADDAndE.mat')% variable called 'a'
% load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksAndRandomLoops','Exp31_29_4_2016_time_10_45','result.mat'))
load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksAndRandomLoops','resultsTwoTADsPeaksAndRandomLoops.mat'))
% l;oad fitting for the experimental data
load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','ExperimentalData','Fitting','fittedEncounterExpData.mat'))

% Collect simulation results into one structure

fittedBetaSim = zeros(numel(r),307);
for rIdx = 1:numel(r)
    fittedBetaSim(rIdx,:)= r(rIdx).fittedBeta;
end

lowerBound        = min(fittedBetaSim);
upperBound        = max(fittedBetaSim);
meanFittedBetaSim = mean(fittedBetaSim);
% Calculate the difference between the mean and the experimental fitted
% beta 
inds                = ~isnan(fittedEncounterExpData.fittedBeta) & fittedEncounterExpData.fittedBeta>0.1 & fittedEncounterExpData.fittedBeta<1.5; % valid inds
normBetaFitVsExpData = norm((meanFittedBetaSim(inds)-fittedEncounterExpData.fittedBeta(inds)),1)/norm(fittedEncounterExpData.fittedBeta(inds),1);

% Figure properties 
fontSize   = 50;
fontName   = 'Arial';
lineWidth  = 4; 
fig4D      = figure; 
ax4D       = axes('Parent',fig4D,'FontSize',fontSize,'FontName',fontName,...
                  'LineWidth',lineWidth,'NextPlot','Add');
patch([1:307, 307:-1:1],[upperBound, fliplr(lowerBound)],'b','FaceAlpha',0.3,'EdgeColor','none',...
    'DisplayName','Error bounds')
plot(ax4D,1:307,fittedEncounterExpData.fittedBeta,'LineWidth',lineWidth,'DisplayName','Experimental data'),
plot(ax4D, 1:307,meanFittedBetaSim,'LineWidth',lineWidth,'DisplayName','Simulation'),
xlabel(ax4D, 'Monomer position');
ylabel(ax4D, 'Decay exponent')
set(ax4D,'XLim',[1 307])
l = legend(get(ax4D,'Children'));
set(l,'Box','off')


% Compare the encoutner probability matrix simulation vs exp data

% get simulation EP matrix 
simEFMatrix = zeros(307);
for rIdx = 1:numel(r)
    simEFMatrix = simEFMatrix+ r(rIdx).encounterFreqMat;    
end

simEPMatrix = zeros(307);
for bIdx = 1:307;
  simEPMatrix(bIdx,:)= simEFMatrix(bIdx,:)./a.SumIgnoreNaN(simEFMatrix(bIdx,:));
end

simEPFiltered = medfilt2(simEFMatrix,[10 10]);

for bIdx = 1:307;
    simEPFiltered(bIdx,:) = simEPFiltered(bIdx,:)./sum(simEPFiltered(bIdx,:));
end

% Get experimental EP matrix 
bRange.bead1 = 1:307;
bRange.bead2 = 1:307;
[osExp,tsExp,expEPMatrix] = a.GetEncounterProbabilityMatrix(bRange,'Average');

expEPFiltered = medfilt2(a.encounterMatrix.average,[10 10]);
for bIdx = 1:307
    expEPFiltered(bIdx,:) = expEPFiltered(bIdx,:)./a.SumIgnoreNaN(expEPFiltered(bIdx,:));
end

% Calculate the normalized norm of the difference between EP matrices
% remove nan
nanIndsFiltered      = isnan(expEPFiltered); % find nan positions in the matrix 
nanInds              = isnan(expEPMatrix);
% t1Inds               = expEPMatrix<0.00001;
% t2Inds               = simEPMatrix<0.00001;
% expEPMatrix(t1Inds)  = 0;
% simEPMatrix(t2Inds)  = 0;
expEPMatrix(nanInds) = 0; % place zeros at nan positions 
simEFMatrix(nanInds) = 0; % match nan positions in Simulation matrix 
expEPFiltered(nanIndsFiltered) = 0;
simEPFiltered(nanIndsFiltered) = 0;

% pass a median filter on the matrices with window corresponding to 30kb
% (i.e size 10)


% Calculate matrix norms 
expVsSimEpNorm         = norm(expEPMatrix-simEPMatrix,2)/norm(expEPMatrix,2)
% filtered 
expVsSimEPNormFiltered = norm(expEPFiltered-simEPFiltered,2)/norm(simEPFiltered,2)


figure, subplot(2,1,1),imagesc(expEPMatrix), title('experimental EP matrix')
subplot(2,1,2),imagesc(expEPFiltered), title('experimental EP matrix median filt')

figure, subplot(2,1,1), imagesc(simEPMatrix),title('simulated EP matrix')
subplot(2,1,2), imagesc(simEPFiltered), title('simulated EP matric filtered')

%--------------------------------
% Calculate the one sided encounter probability and cumulative distribution
osSimEPMatrix  = zeros(307,306);
osExpEPMatrix  = zeros(307,306);
simEPCDF       = zeros(307,306);
expEPCDF       = zeros(307,306);
cdfMaxDistance = zeros(307,1);
expEFMatrix    = a.encounterMatrix.average;
for bIdx = 1:307
    leftSim  = [fliplr(simEFMatrix(bIdx,1:bIdx-1)), zeros(1,307 -bIdx)];
    rightSim = [simEFMatrix(bIdx,(bIdx+1):307), zeros(1,bIdx-1)];
%     nzOSSim  =  2-sum([leftSim;rightSim]==0,1);
    osSimEPMatrix(bIdx,:) = sum([leftSim;rightSim],1)./2;%  nzOSSim;
    osSimEPMatrix(bIdx,:) = osSimEPMatrix(bIdx,:)./a.SumIgnoreNaN(osSimEPMatrix(bIdx,:));
    % Calculate the cumulative CDF for each row 
    % Simulation:
    simEPCDF(bIdx,:)      = cumsum(osSimEPMatrix(bIdx,:));
    
    
    % Experimental: 
    leftExp  = [fliplr(expEFMatrix(bIdx,1:bIdx-1)), zeros(1,307 -bIdx)];
    rightExp = [expEFMatrix(bIdx,(bIdx+1):307), zeros(1,bIdx-1)];
    nzOSExp  =  2-sum([leftExp;rightExp]==0,1);
    osExpEPMatrix(bIdx,:) = a.SumIgnoreNaN([leftExp;rightExp])./ nzOSExp;
    osExpEPMatrix(bIdx,:) = osExpEPMatrix(bIdx,:)./a.SumIgnoreNaN(osExpEPMatrix(bIdx,:));
            
    % Experimental data:
    expEPCDF(bIdx,:)      = cumsum(osExpEPMatrix(bIdx,:)./a.SumIgnoreNaN(osExpEPMatrix(bIdx,:)));
    % calculate the maximal difference between the two CDFs
    cdfMaxDistance(bIdx)  = max(abs(simEPCDF(bIdx,:)-expEPCDF(bIdx,:)));
end

figure, 
plot(1:307,cdfMaxDistance,'LineWidth',lineWidth)
title('maximal difference |CDF_{exp}-CDF_{Sim}|','FontSize',fontSize)
set(gca,'Box','off','LineWidth',lineWidth,'FontSize',fontSize,'XLim',[1 307],'NextPlot','Add')
% plot the average of the maximal CDF differnce
meanCDFMaxDistance = a.MeanIgnoreNaN(cdfMaxDistance);
plot([1 307],[meanCDFMaxDistance meanCDFMaxDistance],'r--','LineWidth',lineWidth,'DisplayName','Average')
xlabel('Monomer position','FontSize',fontSize);
ylabel('D_{Max}','FontSize',fontSize);



%% Figure: cdf of the Average one sided encounter prob Simulation Vs. Exp 
validRowsExp  = 285;
validRowsSim  = 307;
simAverageCDF = cumsum(a.SumIgnoreNaN(osSimEPMatrix)./validRowsExp);
expAverageCDF = cumsum(a.SumIgnoreNaN(osExpEPMatrix)./validRowsExp);
figure, plot(simAverageCDF,'DisplayName','simulation','LineWidth',5);
hold on,
plot(expAverageCDF ,'DisplayName','experimental data','LineWidth',5);
ylabel('CDF','FontSize',fontSize)
xlabel('Distance [monomer]','FontSize',fontSize)
l = legend(get(gca,'Children'));
set(l,'Box','off','FontSize',fontSize)
% Calculate the maximal difference between the CDFs 
[dMax, dMaxInd] = max(abs(simAverageCDF-expAverageCDF));
% dMaxInd = dMaxInd+10;
plot(dMaxInd,simAverageCDF(dMaxInd),'o','MarkerEdgeColor','k')
plot(dMaxInd,expAverageCDF(dMaxInd),'o','MarkerEdgeColor','k')
text(150,0.4,sprintf('%s%s','D_{Max}= ', num2str(dMax)),'FontSize',fontSize)
set(gca,'LineWidth',lineWidth,'FontSize', fontSize,'XLim',[1 307],'YLim',[0 1.05],'Box','off')

% perform KS test on the CDFs 
c_alpha = 1.97; % test at 0.001 confidence level see wikipedia 
if (dMax>c_alpha*sqrt(2*307/307^2))
    disp('the null hypothesis is rejected. CDF comes from different distributions')
else
    disp('The null hypothesis is NOT rejected. CDF are similar')
end

[h,pVal,ks2stat] = kstest2(a.SumIgnoreNaN(osSimEPMatrix)./validRowsExp,a.SumIgnoreNaN(osExpEPMatrix)./validRowsExp,'Alpha',0.001)

