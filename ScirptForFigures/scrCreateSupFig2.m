%scrCreateSupFig2A
% make sure you're in the code folder 
close all 
load('savedAnalysisTADDAndE.mat')% variable called 'a'
% load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksAndRandomLoops','Exp31_29_4_2016_time_10_45','result.mat'))
load(fullfile(pwd,'..','..','OwnCloud','PolymerChainDynamicsResults','savedResults','TwoTADsPeaksAndRandomLoops','resultsTwoTADsPeaksAndRandomLoops.mat'))
simEFMatrix = zeros(307);
for rIdx = 1:numel(r)
    simEFMatrix = simEFMatrix+ r(rIdx).encounterFreqMat;
end
simEPMatrix = zeros(307);
for bIdx = 1:307;
    simEPMatrix(bIdx,:) = simEFMatrix(bIdx,:)./sum(simEFMatrix(bIdx,:));
end

fontSize  = 30;
lineWidth = 3;
xLim      = [1 307];
yLim      = [1 307];
zLim      = [0 0.2];

% Show the 3d encounter prob matrix 
% experimental data 
fig1         = figure('FileName','expEPMatrix');
ax1          = axes('Parent',fig1,'FontSize',fontSize,'LineWidth',lineWidth);
bRange.bead1 = 1:307;
bRange.bead2 = 1:307;
[os,ts,expEPMatrix] = a.GetEncounterProbabilityMatrix(bRange,'Average');
surf(expEPMatrix,'LineStyle','none'), cameratoolbar
% xlabel(ax1,'Monomer number','fontSize',fontSize);
% ylabel(ax1,'Monomer number','fontSize',fontSize);
% zlabel(ax1,'Encounter prob.','fontSize',fontSize);
set(ax1,'ZLim',zLim,'XLim',xLim,'YLim',yLim,'LineWidth',lineWidth,'FontSize',fontSize,...
    'GridLineStyle','none',...
    'CameraViewAngle',9.256133109732078,...
    'CameraUpVector',[-800, 63,0.8],...
    'CameraPosition',[2300, 872, 1.27],...
    'CameraTarget',[154 154 0.125]);
view(ax1,[102.4990234375 35.6201171875]);

% Display the simulation EP matrix 
fig2 = figure('FileName','simEPMatrix');
ax2  = axes('Parent',fig2,'FontSize',fontSize,'LineWidth',lineWidth);
surf(ax2,simEPMatrix,'LineStyle','none'),
% title(ax2,'Simlation','FontSize',fontSize)
% xlabel(ax2,'Monomer number','fontSize',fontSize);
% ylabel(ax2,'Monomer number','fontSize',fontSize);
% zlabel(ax2,'Encounter prob.','fontSize',fontSize);
set(ax2,'ZLim',zLim,'XLim',xLim,'YLim',yLim,'LineWidth',lineWidth,'FontSize',fontSize,...
    'GridLineStyle','none',...
    'CameraViewAngle',9.256133109732078,...
    'CameraUpVector',[-800, 63,0.8],...
    'CameraPosition',[2300, 872, 1.27],...
    'CameraTarget',[154 154 0.125]);
view(ax2,[102.4990234375 35.6201171875]);
cameratoolbar
