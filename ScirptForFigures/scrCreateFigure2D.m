% scrCreateFigure2D

% load result for one TAD
% make sure the working directory is the one containing this script

load(fullfile(pwd,'..','..','Results','resultOneTAD307Beads.mat'))

for i=1:11; meanFittedBeta(i) = mean(result(i).fittedBeta(103:203));end;
% fittedBetaE = result.fittedBeta(107:307);
% Create figure
figure1 = figure('InvertHardcopy','off','Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1,'LineWidth',3,'FontSize',30,'YLim',[0 1.8]);

hold(axes1,'all');

line('XData',[result.connectivityTAD1],'YData',meanFittedBeta+0.1,'Marker','o',...
    'MarkerFaceColor','b',...
    'Color','b','MarkerSize',8,'LineWidth',4,'Parent',axes1)

xlabel('Connectivity \xi','FontSize',40,'Parent',axes1)
ylabel('Average \beta Exp.','FontSize',40,'Parent',axes1)
